#! /usr/bin/env python

import sys

class Assembler():
    def __init__(self):
        self.macros = {}
        self.labels = {}
        self.stack = []
        self.tokens = []
        self.pc = 0x100
        self.unresolved = []
        self.output = ''

    def writeWord(self, val):
        self.output += chr(val & 0xff)
        self.output += chr(val >> 8)
        self.pc += 1

    def getNextToken(self):
        try:
            ret = self.tokens[0]
            self.tokens = self.tokens[1:]
            return ret
        except:
            return None

    def eval(self, token):
        if token.startswith('('):
            t = self.getNextToken()
            while not t.endswith(')'):
                t = self.getNextToken()
        elif token.startswith(':'):
            self.macros[token[1:]] = []
            t = self.getNextToken()
            while t != ';':
                self.macros[token[1:]].append(t)
                t = self.getNextToken()
        elif token == 'WORD':
            self.writeWord(self.stack.pop())
        elif token == 'SWAP':
            a = self.stack.pop()
            b = self.stack.pop()
            self.stack += [a, b]
        elif token == 'DUP':
            a = self.stack.pop()
            self.stack += [a, a]
        elif token == 'IMPORT':
            tokens = file(self.getNextToken()).read().split()
            self.tokens = tokens + self.tokens
        elif token == 'TXT':
            txt = file(self.getNextToken()).read()
            for c in txt:
                self.writeWord(ord(c))
            self.writeWord(0x00)
        elif token in self.macros:
            for t in self.macros[token]:
                self.eval(t)
        elif token.endswith(':'):
            self.labels[token[:-1]] = self.pc
        elif token.startswith('0x'):
            self.stack.append(int(token, 16))
        else:
            try:
                self.stack.append(int(token))
            except:
                if token in self.labels:
                    self.stack.append(self.labels[token])
                else:
                    self.stack.append(0)

    def assemble(self, name):
        print "pass 1"
        self.__init__()
        data = file(name).read()
        self.tokens = data.split()
        token = self.getNextToken()
        while token:
            self.eval(token)
            token = self.getNextToken()
        print "pass 2"
        self.tokens = []
        self.pc = 0x100
        self.stack = []
        self.output = ''
        self.tokens = data.split()
        token = self.getNextToken()
        while token:
            self.eval(token)
            token = self.getNextToken()

if __name__ == '__main__':
    a = Assembler()
    if len(sys.argv) >= 2:
        a.assemble(sys.argv[1])
        file(sys.argv[1] + '.bin', 'w+').write(a.output)
    else:
        print "usage: ./assembler.py [source]"

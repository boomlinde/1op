#! /usr/bin/env python

import sys
import time

class RAM():
    '''
    Simple RAM simulator with function hooks for memory mapped functionality
    '''
    def __init__(self, size=0x10000):
        self.size = size
        self.ram = [0 for x in range(size)]
        self.hooks = []

    def __getitem__(self, index):
        try:
            return self.ram[index]
        except:
            sys.exit(0)

    def __setitem__(self, w_index, r_index):
        noHooks = True
        for hook in self.hooks:
            if hook['index'] == w_index:
                self.ram[w_index] = hook['function'](r_index, w_index, self) & 0xffff
                noHooks = False
        if noHooks:
            self.ram[w_index] = self.ram[r_index] & 0xffff

    def setVal(self, index, val):
        self.ram[index] = val

    def addHook(self, function, index):
        self.hooks.append({'function': function, 'index': index})

class External():
    '''
    Memory mapped functions
    '''

    @staticmethod
    def m_constant(r_index, w_index, ram):
        return r_index

    @staticmethod
    def m_vector(r_index, w_index, ram):
        return ram[ram[r_index]]

    @staticmethod
    def m_add(r_index, w_index, ram):
        return ram[w_index] + ram[r_index]

    @staticmethod
    def m_xor(r_index, w_index, ram):
        return ram[w_index] ^ ram[r_index]

    @staticmethod
    def m_or(r_index, w_index, ram):
        return ram[w_index] | ram[r_index]

    @staticmethod
    def m_and(r_index, w_index, ram):
        return ram[w_index] & ram[r_index]

    @staticmethod
    def m_lshift(r_index, w_index, ram):
        return ram[r_index] << 1

    @staticmethod
    def m_rshift(r_index, w_index, ram):
        return ram[r_index] >> 1

    @staticmethod
    def m_nz(r_index, w_index, ram):
        return int(ram[r_index] != 0)

    @staticmethod
    def m_print(r_index, w_index, ram):
        print ram[r_index]
        return ram[r_index]

    @staticmethod
    def m_printc(r_index, w_index, ram):
        sys.stdout.write(chr(ram[r_index] & 0xff))
        sys.stdout.flush()
        return ram[r_index]

class Machine():
    '''
    Simple one instruction machine, conditional move
    By default:
        RAM size is 0x10000 cells
        Program counter is in RAM cell 0
        Move condition is in RAM cell 1
        Program entry is at RAM cell 0x100
    '''

    def __init__(self, entry=0x100, ram=0x10000, pc=0, condition=1):
        self.pc = pc
        self.ram = RAM(ram)
        self.ram.setVal(self.pc, entry)
        self.condition = condition
        self.ram.setVal(self.condition, 1)

        self.ram.addHook(External.m_constant, 0x10)
        self.ram.addHook(External.m_vector, 0x11)
        self.ram.addHook(External.m_add, 0x20)
        self.ram.addHook(External.m_and, 0x30)
        self.ram.addHook(External.m_or, 0x31)
        self.ram.addHook(External.m_xor, 0x32)
        self.ram.addHook(External.m_lshift, 0x40)
        self.ram.addHook(External.m_rshift, 0x41)
        self.ram.addHook(External.m_print, 0x50)
        self.ram.addHook(External.m_printc, 0x51)
        self.ram.addHook(External.m_nz, 0x60)

    def step(self):
        read = self.ram[self.pc]
        write = self.ram[self.pc] + 1
        self.ram.setVal(self.pc, self.ram[self.pc] + 2)
        if self.ram[self.condition]:
            self.ram[self.ram[write]] = self.ram[read]
        else:
            self.ram.setVal(self.condition, 1)


def loadProgram(name):
    data = file(name).read()
    data = [ord(x) for x in data]
    i = 0
    program = []
    while i < len(data):
        program.append(data[i])
        program[-1] += data[i + 1] << 8
        i += 2
    return program


if __name__ == '__main__':
    if len(sys.argv) >= 2:
        program = loadProgram(sys.argv[1])
    else:
        print "usage: ./1op.py [program]"
        sys.exit(0)
    m = Machine()
    write_p = 0x100
    for cell in program:
        m.ram.setVal(write_p, cell)
        write_p += 1

    while True:
        m.step()
